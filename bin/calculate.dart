import 'dart:io';

import 'package:calculate/calculate.dart' as calculate;

void main(List<String> arguments) {
  print('Enter for a: ');
  int a = int.parse(stdin.readLineSync()!);
  print('Enter for b: ');
  int b = int.parse(stdin.readLineSync()!);
  print('Please choose number for calculate');
  print('1(+) 2(-) 3(*) 4(/) :');

  int symbol = int.parse(stdin.readLineSync()!);

  int result = 0;

  switch (symbol) {
    case 1:
      result = a + b;
      break;
    case 2:
      result = a - b;
      break;
    case 3:
      result = a * b;
      break;
    case 4:
      result = a ~/ b;
      break;
    case 5:
      exit(0);
    default:
  }
  print("Answer is :");
  print("$a $symbol $b : $result");
}
